<?php

/**
 * Form builder. Configure Hypernode.
 * 
 * Forms API Quickstart Guide: http://drupal.org/node/751826
 * notes for 2nd button: http://drupal.org/node/1533050
 */
include 'hypernode.import.inc';
include 'hypernode.hyperlink.inc';


function hypernode_administer($form, &$form_state) {
  $form['hypernode_import']= array(
    '#title' => t('Import text data to Hypernode content types'),
    '#type' => 'fieldset',
    '#description' => t('Hypernode will import existing text data.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $path_to_data = drupal_get_path('module', 'hypernode') . '/hypernode.import.data';
  $form['hypernode_import']['hypernode_source_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Source path for the text files (containing the media and pages directories)'),
    '#default_value' => variable_get('hypernode_source_path', $path_to_data),
    '#size' => 90,
    '#description' => t('Directory structure and text data format approximate DokuWiki format. See README for details.'),    
  );
  $form['hypernode_import']['hypernode_url_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Prefix to place on the URL Alias for all nodes (optional)'),
    '#default_value' => variable_get('hypernode_url_prefix', 'texts'), 
    '#size' => 90,
    '#description' => t('Use if you wish your URLs to begin with a distinct term (no trailing slash!).'),    
  );
  $form['hypernode_import']['hypernode_convert_dokuwiki'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('hypernode_convert_dokuwiki', FALSE),
    '#title' => t('Convert imported Dokuwiki text to HTML'), 
    '#description' => t('Full conversion is not yet available but many commonly used Dokuwiki markup elements are converted.'),
  );
  $form['hypernode_import']['hypernode_delete_content'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('hypernode_delete_content', FALSE),    
    '#title' => t('Delete existing hypernode content'),
    '#description' => t('If checked, all existing hypernode content will be deleted before importing.'),
  ); 
  $form['hypernode_import']['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Import text data'),
    '#validate' => array('hypernode_import_validate'),
    '#submit' => array('hypernode_import_submit'),
  );
  $form['hypernode_hyperlink']= array(
    '#title' => t('Hyperlink hypernode data'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  

  $val = "";
  $val = $val . "texts/asv => texts/mbd" . "\r\n";
  $val = $val . "texts/asv => texts/rw" . "\r\n";
  $val = $val . "texts/mbd => texts/asv" . "\r\n";
  $val = $val . "texts/mbd => texts/mbd" . "\r\n";
  $val = $val . "texts/mbd => texts/rw" . "\r\n";  
  $val = $val . "texts/rw => texts/rw" . "\r\n";
  $val = $val . "texts/rw => texts/asv" . "\r\n";
  $val = $val . "texts/rw => texts/mbd" . "\r\n";  
  $form['hypernode_hyperlink']['hypernode_hyperlink_mapping'] = array(
    '#type' => 'textarea',
    '#title' => t('Mapping of dictionary nodes to text nodes'),
    '#default_value' => variable_get('hypernode_hyperlink_mapping', $val),
    '#description' => t("Add links to the nodes in the left domain that have text which contains the title of nodes in the right domain."),    
   );

   $form['hypernode_hyperlink']['hypernode_hyperlink_ignore_nodes'] = array(
    '#type' => 'textarea',
    '#title' => t('Nodes to ignore for hyperlinking'),
    '#default_value' => variable_get('hypernode_hyperlink_ignore_nodes', 'texts/mbd/on, texts/mbd/put, texts/mbd/so, texts/mbd/son'),
    '#description' => t("Enter nodes that should not be linked to, separated by commas or spaces."),    
   );
   $form['hypernode_hyperlink']['hypernode_hyperlink_delimiter_before'] = array(
    '#type' => 'textfield',
    '#title' => t('Acceptable characters that may precede a linked term'),
    '#default_value' => variable_get('hypernode_hyperlink_delimiter_before', '^^ ^^,^^"^^(^^\n^^\r^^'), 
    '#size' => 90,
    '#description' => t('Surround delimiting characters with ^^. Use escapes as necessary.'),    
   );
   $form['hypernode_hyperlink']['hypernode_hyperlink_delimiter_after'] = array(
    '#type' => 'textfield',
    '#title' => t('Acceptable characters that may follow a linked term'),    
    '#default_value' => variable_get('hypernode_hyperlink_delimiter_after', '^^ ^^,^^"^^.^^;^^:^^?^^)^^\n^^\r^^'), 
    '#size' => 90,
    '#description' => t('Example: include the terms phone, (phone), phone:, phone? and "phone", but do not include the term telephone'),    
   );   
  $form['hypernode_hyperlink']['submit_button2'] = array(
    '#type'=> 'submit',
    '#value' => t('Hyperlink the hypernode data'),
    '#validate' => array('hypernode_hyperlink_validate'),
    '#submit' => array('hypernode_hyperlink_submit'),
   );
 
   return ($form);
 }
 
/**
 * Process Import
 */
function hypernode_import_validate ($form, $form_state) {
}

function hypernode_import_submit ($form, $form_state) {
  hypernode_import_run($form, $form_state);
}


 
/**
 * Process Hyperlink
 */
function hypernode_hyperlink_validate ($form, $form_state) {

}
function hypernode_hyperlink_submit ($form, $form_state) {
  hypernode_hyperlink_run($form, $form_state);
}

/**
 * Process the entire form (is this necessary?)
 */
function hypernode_administer_validate($form, &$form_state) {

}
function hypernode_administer_submit ($form, $form_state) {

}




