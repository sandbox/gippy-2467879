<?php

  $hn_files_path;
  $hn_convert_dokuwiki;
  $hn_delete_content;
  
  $hn_files_media_path;
  $hn_files_pages_path;
  $hn_url_prefix;
  
  $hn_count_pages;
  $hn_count_adds;
  $hn_count_updates;
  $hn_count_deletes;

  $hn_count_links;
  $input_format_machine_name;  
  
  // page data
  $hn_url_alias;
  $hn_namespace;
  $hn_node_title;
  $hn_file_content;
  
/**
 * Import text files into nodes.
 */
function hypernode_import_run ($form, $form_state) {
  GLOBAL $hn_delete_content;
  GLOBAL $hn_files_pages_path;
  GLOBAL $hn_count_pages;
  GLOBAL $hn_count_adds;
  GLOBAL $hn_count_updates;
  GLOBAL $hn_count_deletes;  
  GLOBAL $hn_count_links;
  
  if(hypernode_import_set_vars($form, $form_state) == FALSE) {
    return FALSE;    
  }
  
  if(hypernode_import_check_input($form, $form_state) == FALSE) {
    return FALSE;    
  }
        
  if ($hn_delete_content == TRUE){
    if(hypernode_import_delete_content() == FALSE) {
    return FALSE;
    }
  }  
  
  if(hypernode_import_scan_pages($hn_files_pages_path) == FALSE) {
    return FALSE;
  }
  
  drupal_set_message(t("Import completed. Pages:" . $hn_count_pages . " Adds:" . $hn_count_adds . " Updates:" . $hn_count_updates . " Deletes:" . $hn_count_deletes .  " Links: " . $hn_count_links));
  return TRUE;
}

/**
 * Set variables
 */
function hypernode_import_set_vars($form, $form_state) {
  GLOBAL $hn_files_path;
  GLOBAL $hn_convert_dokuwiki;
  GLOBAL $hn_delete_content;
  
  GLOBAL $hn_files_media_path;
  GLOBAL $hn_files_pages_path;  
  GLOBAL $hn_url_prefix;
  
  GLOBAL $input_format_machine_name;
  
  $hn_files_media_path = "";
  $hn_files_pages_path = "";
  $hn_url_prefix = "";
  
  $input_format_machine_name ="";
  
  $hn_files_path = $form_state['values']['hypernode_source_path'];
  variable_set('hypernode_source_path', $hn_files_path);
  
  $hn_url_prefix = $form_state['values']['hypernode_url_prefix'];
  variable_set('hypernode_url_prefix', $hn_url_prefix);

  $hn_convert_dokuwiki = $form_state['values']['hypernode_convert_dokuwiki'];
  variable_set('hypernode_convert_dokuwiki', $hn_convert_dokuwiki);
  
  $hn_delete_content = $form_state['values']['hypernode_delete_content'];
  variable_set('hypernode_delete_content', $hn_delete_content);

  if(strlen($hn_url_prefix) > 0){
    $hn_url_prefix = $hn_url_prefix . "/";
  }
  return TRUE;
}


/**
 * Validate user input
 * 
 * The user must enter a valid directory where source files are located.
 * The directory must have a 'pages' subdirectory where .txt files for importing into nodes are located
 * The directory must have a 'media' subdirectory where media files such as .pdf, .jpg are located
 *
 * This is patterned after the Dokuwiki file structure and should enable seemless importing of Dokuwiki data
 */
function hypernode_import_check_input($form, $form_state) {
  GLOBAL $hn_files_path;
  GLOBAL $hn_files_media_path;
  GLOBAL $hn_files_pages_path;

  if (!is_dir($hn_files_path)) {
    drupal_set_message(t('Unable to open the hypernode source path'), 'error');
    return FALSE;
  }

  $hn_files = scandir($hn_files_path);
  foreach($hn_files as $hn_filename) {
    if(substr($hn_filename, 0, 1) <> ".") {
      switch($hn_filename){
        case "media":
          $hn_files_media_path = $hn_files_path . "/" . $hn_filename;
          break;
        case "pages":
          $hn_files_pages_path = $hn_files_path . "/" .  $hn_filename;
          break;
        default:
          drupal_set_message(t("\"" . $hn_filename . "\" directory is not media or pages and is being skipped"), 'warning');
      }
    }
  }
  if( ($hn_files_media_path == "") or (!is_dir($hn_files_media_path)) ){
    drupal_set_message(t('Media directory not found'), 'error');
    return FALSE;
  }
  if( ($hn_files_pages_path == "") or (!is_dir($hn_files_pages_path)) ){
    drupal_set_message(t('Pages directory not found'), 'error');
    return FALSE;
  }
  
  return TRUE; 
}

/**
 * delete all hypernode pages
 */
function hypernode_import_delete_content() {
  GLOBAL $hn_count_deletes;
  $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
  $result = db_query($sql, array(':type' => 'hypernode'));
  foreach ($result as $row) {
    $nid = $row->nid;
    $node = node_load($nid);
    unset($node->field_hypernode_namespace[$node->language][0]);
    node_delete($nid);
    $hn_count_deletes ++;
  }
  return TRUE;
}


/**
 * check for bad multibyte encoding
 * implemented to check for: "Warning: htmlspecialchars() [function.htmlspecialchars]:
 *   Invalid multibyte sequence in argument in check_plain() (line 1572 ..."
 * http://www.php.net/manual/en/function.mb-check-encoding.php
 * thanks, javacl6
 */
function hypernode_check_utf8($str) { 
    $len = strlen($str); 
    for($i = 0; $i < $len; $i++){ 
        $c = ord($str[$i]); 
        if ($c > 128) { 
            if (($c > 247)) return false; 
            elseif ($c > 239) $bytes = 4; 
            elseif ($c > 223) $bytes = 3; 
            elseif ($c > 191) $bytes = 2; 
            else return false; 
            if (($i + $bytes) > $len) return false; 
            while ($bytes > 1) { 
                $i++; 
                $b = ord($str[$i]); 
                if ($b < 128 || $b > 191) return false; 
                $bytes--; 
            } 
        } 
    } 
    return true; 
} // end of hypernode_check_utf8 

/**
 * recursively scan through the source directory
 * http://php.net/manual/en/function.scandir.php - thanks, Fazle
 */
function hypernode_import_scan_pages($files_pages_path){
  GLOBAL $hn_convert_dokuwiki;
  GLOBAL $hn_files_pages_path;
  GLOBAL $hn_count_pages;  
  GLOBAL $hn_url_prefix;

  // Page data. these will be passed to the markup function and to the update function  
  GLOBAL $hn_url_alias;
  GLOBAL $hn_namespace;
  GLOBAL $hn_node_title;
  GLOBAL $hn_file_content;
     
  $filelist = @scandir($files_pages_path);
  if(!empty($filelist)) {
    foreach($filelist as $f){
      if (substr($f, 0, 1) == ".") {
      // do nothing. bypass hidden files and directories.
      }
      elseif(is_dir($files_pages_path . "/" . $f)) {
      // is a directory. drill down.
        hypernode_import_scan_pages($files_pages_path . "/" . $f);
      }
      else {
        // open the file and get content
	    $hn_file_path = $files_pages_path . "/" . $f;
	    $hn_file_handle = @fopen($hn_file_path, "rb");
	    if ($hn_file_handle == FALSE) {
	      drupal_set_message("failed to open " . $hn_file_path);
        continue;
	    }

      $hn_file_content = fread($hn_file_handle, filesize($hn_file_path));
      if (hypernode_check_utf8($hn_file_content) == FALSE){
        drupal_set_message(t("File with invalid multi-byte encoding was skipped:" . $hn_file_path), 'error');
        continue;
      }
      
        $hn_count_pages ++;
            
        // strip .txt from filename
        $f = substr($f, 0, strlen($f) - 4);
             
        // set the url alias for this node
        if(strlen($files_pages_path) == strlen($hn_files_pages_path)) {
          $hn_url_alias = $hn_url_prefix . $f;
        }
        else {
          $hn_url_alias = $hn_url_prefix . substr($files_pages_path, strlen($hn_files_pages_path)+1) . "/" . $f;
        }
        // $hn_url_alias = str_replace("_", "-", $hn_url_alias); moved to conversion functions
            
        // make note of the namespace for this page (without the url prefix)
        $hn_namespace = substr($files_pages_path, strlen($hn_files_pages_path)+1);
        // drupal_set_message($hn_namespace);
            
        // set the title for this node. This is presumably the first line in the text.
        fseek($hn_file_handle, 0);
        if (!feof($hn_file_handle)){
          $hn_node_title = fgets($hn_file_handle);
        }
        $hn_node_title = preg_replace('/[\x00-\x1F\x7F]/', '', $hn_node_title);
        $html_reg = '/<+\s*\/*\s*([A-Z][A-Z0-9]*)\b[^>]*\/*\s*>+/i';
        $hn_node_title = preg_replace( $html_reg, '', $hn_node_title);
        $hn_node_title = trim(str_replace("=", "", $hn_node_title));
        $hn_node_title = trim(str_replace("&nbsp;", "", $hn_node_title));
        if ($hn_node_title == ""){
          $hn_node_title = $f;
        }
        
        // convert data if requested
        if($hn_convert_dokuwiki == TRUE){
          if(hypernode_import_convert_content_dokuwiki() == FALSE){
            return FALSE;
          }
        }
        
        // add/update a node based on the content of this text file
        if(hypernode_import_node_update() == FALSE){
          return FALSE;
        }
      }
    }
  }
    return TRUE;
}



/**
 * Convert content from Dokuwiki to HTML
 */
function hypernode_import_convert_content_dokuwiki(){
  GLOBAL $hn_url_prefix;

  GLOBAL $hn_count_links;
  
  // Page data
  GLOBAL $hn_url_alias;
  GLOBAL $hn_namespace;
  GLOBAL $hn_node_title;
  GLOBAL $hn_file_content;
  
  // set url alias to hyphens. Note that links are also converted below to use hyphens.
  $hn_url_alias = str_replace("_", "-", $hn_url_alias);
    
  // convert as much as possible of the content from wiki to html markup with regex
  // note: (.*?) will stop at the first match (lazy), but (.*) will stop at the final match (greedy)
  $hn_file_content = preg_replace( '/======(.*?)======/', '<h1>$1</h1>', $hn_file_content);  // ====== h1 ======
  $hn_file_content = preg_replace( '/=====(.*?)=====/', '<h2>$1</h2>', $hn_file_content);  // ===== h2 =====
  
  // hack to change //italics// to <em>italics/em> but prevent it from matching http:// or https://
  // I would appreciate someone showing me how to do this with one regex expression
  // confirm by reviewing wiki/mbd/jerusalem and wiki/start
  $hn_file_content = preg_replace( '/http:\/\//', 'http:!dRuPaL!RoCkS!', $hn_file_content);       // hide http:// from being matched
  $hn_file_content = preg_replace( '/https:\/\//', 'https:!dRuPaL!RoCkS!', $hn_file_content);     // hide https:// from being matched
  $hn_file_content = preg_replace( '/\/\/(.*?)\/\//', '<em>$1</em>', $hn_file_content);           // match //italics//
  $hn_file_content = preg_replace( '/http:!dRuPaL!RoCkS!/', 'http://', $hn_file_content);         // change back to http://
  $hn_file_content = preg_replace( '/https:!dRuPaL!RoCkS!/', 'https://', $hn_file_content);       // change back to https://
  
  $hn_file_content = preg_replace( '/\*\*(.*?)\*\*/', '<b>$1</b>', $hn_file_content);             // **bold**
  
  
  // convert links.
  // From internal link: [[namespace:link|text]] to: <a href=" + url_prefix + namespace + link "> text </a>
  // From external link: [[http://<www.php.net|text]] to: <a href="http://<www.php.net"> text </a>
  $tag_pos_beg = 0;
  $tag_pos_end = 0;
  
  while (1) {
    $tag_pos_beg = stripos($hn_file_content, "[[", $tag_pos_end);
    if ($tag_pos_beg === FALSE){
        break;
    }
    $tag_pos_end = stripos($hn_file_content, "]]", $tag_pos_beg);
    if ($tag_pos_end === FALSE){
        break;
    }
       
    // opening and closing brackets were found. now get the content
    $tag_link = substr($hn_file_content, $tag_pos_beg +2, $tag_pos_end - $tag_pos_beg -2);
    $tag_link_with_brackets = substr($hn_file_content, $tag_pos_beg, $tag_pos_end - $tag_pos_beg + 2);
    
    // extract the link text. if there is no link text, then copy the link itself to the link text
    $tag_text = "";
    $tag_pos_text = strpos($tag_link, "|");
    if($tag_pos_text > 0){
      $tag_text = substr($tag_link, $tag_pos_text +1);
      $tag_link = substr($tag_link, 0, $tag_pos_text);
    }
    else{
      if ( (substr($tag_link, 0, 7) == 'http://')  or (substr($tag_link, 0,8) == 'https://') ){
        $tag_text = $tag_link;    // external link
      }
      else{
        $tag_text = substr($tag_link, strrpos($tag_link, ":"));  // internal link
        $tag_text = ltrim($tag_text, ":");  
      }            
    }
    
    // if the link begins with . or .: of if it has no : before a "|" then prepend the namespace
    if ((substr($tag_link, 0, 1) == ".")
        or (substr($tag_link, 0, 2) == ".:") 
        or (strpos($tag_link, ":") === FALSE) ){
        $tag_link = $hn_namespace . ":" . $tag_link;
    }
    
    // change wiki namespace separator (:) to html directory separator (/)
    $tag_link = str_replace(":", "/", $tag_link);
    $tag_link = str_replace("http///", "http://", $tag_link);   // hack
    $tag_link = str_replace("https///", "https://", $tag_link); // hack
    
    // convert the link to lowercase and fill spaces and underscores with hyphens
    $tag_link = strtolower($tag_link);
    $tag_link = str_replace(" ", "-", $tag_link);
    $tag_link = str_replace("_", "-", $tag_link);
    
    // assemble the html link and replace the wiki link with the html link
    if (stripos($tag_link, "http://") === FALSE){
      $tag_link_complete = "<a href=\"/" . $hn_url_prefix . $tag_link . "\">" . $tag_text . "</a>"; 
    }
    else {
      $tag_link_complete = "<a href=\"" . $tag_link . "\">" . $tag_text . "</a>";    
    }
    // replace the link. note that this replaces all instances, not just the one that is being processed.
    $hn_count_links = $hn_count_links + substr_count($hn_file_content, $tag_link_with_brackets);
    $hn_file_content = str_replace($tag_link_with_brackets, $tag_link_complete, $hn_file_content);
    // drupal_set_message($tag_link . "--" . $tag_text . "--" . $tag_link_complete . "--" . $tag_pos_beg . "--" . $tag_pos_end);   
  }
 
 
 // convert unordered lists from wiki to html
  $tag_pos_beg = 0;
  $tag_pos_end = 0;
  $tag_tally = 0;
  
  while (1) {
    $tag_pos_beg = stripos($hn_file_content, "\n  * ", $tag_pos_end);
    if ($tag_pos_beg === FALSE){
        if ($tag_tally > 0){
           // $hn_file_content = substr_replace($hn_file_content, "</li></ul>", $tag_pos_end -1, 1);
           $hn_file_content = substr_replace($hn_file_content, "</ul>", $tag_pos_end + 6, 0);
        }
        break;
    }
    $tag_pos_end = stripos($hn_file_content, "\n", $tag_pos_beg + 1);
    if ($tag_pos_end === FALSE){
        break;
    }
    
    $tag_text = substr($hn_file_content, $tag_pos_beg + 5, $tag_pos_end - $tag_pos_beg - 4);
    if ($tag_tally == 0){
        $tag_text = "<ul><li>" . $tag_text . "</li>";
    }
    else{
        $tag_text = "<li>" . $tag_text . "</li>";
    }
    $tag_tally ++;
    
    $hn_file_content = substr_replace($hn_file_content, $tag_text, $tag_pos_beg +1, $tag_pos_end - $tag_pos_beg -1);
    
  }  
  return TRUE;
}

/**
 * Add/Udpate Node
 * http://drupal.org/node/1388922
 * http://fooninja.net/2011/04/13/guide-to-programmatic-node-creation-in-drupal-7/
 */
function hypernode_import_node_update(){
  global $user;
  GLOBAL $hn_files_pages_path;  
  GLOBAL $hn_url_prefix;
  
  GLOBAL $hn_count_adds;
  GLOBAL $hn_count_updates;
  GLOBAL $hn_count_deletes;  
  GLOBAL $hn_count_links;
  
  // Page data
  GLOBAL $hn_url_alias;
  GLOBAL $hn_namespace;
  GLOBAL $hn_node_title;
  GLOBAL $hn_file_content;
  
  GLOBAL $input_format_machine_name;  
  
  // determine if a new node
  $normal_path = drupal_get_normal_path($hn_url_alias);
  
  if ($normal_path == $hn_url_alias){
    $hn_update_mode = 'add';
    $node = new stdClass();
    $node->type = "hypernode";
    node_object_prepare($node);
    $hn_count_adds ++;
  }
  else{
    $hn_update_mode = 'change';
    $hn_nid = substr($normal_path, strlen("node/"));  //example: node/2918
    $node = node_load($hn_nid);
    if($node == FALSE){
      drupal_set_message(t("Error in updating node:" . $hn_nid . " from URL alias:" . $hn_url_alias), 'error');
      return TRUE;
    }
    $hn_count_updates ++;
  }
  
  // BUG: problem with machine name for input format
  // If a D7 site was upgraded from D6, then the machine name for input formats is a sequential number instead of 'full_html'
  // http://www.group42.ca/creating_and_updating_nodes_programmatically_in_drupal_7
  // http://commerceguys.com/blog/installing-drupal-commerce-site-upgraded-drupal-6

  if ($input_format_machine_name == ""){
    $input_format_machine_name = 'full_html';  // default D7 machine name
    $sql = 'SELECT format FROM {filter_format} WHERE name = :name';
    $result = db_query($sql, array(':name' => 'Full HTML'));
    foreach ($result as $record){     
      $input_format_machine_name = strval($record->format);
      $input_format_machine_name = strval($input_format_machine_name);
      drupal_set_message("input format is: " . $input_format_machine_name);
    }    
  }  
  
  
  $node->title = $hn_node_title;
  $node->language = LANGUAGE_NONE;
  $node->uid = $user->uid; 
  $node->status = 1; //(1 or 0): published or not
  $node->promote = 0; //(1 or 0): promoted to front page
  $node->comment = 1; //2 = comments on, 1 = comments off
  $node->sticky = 0; // Display top of page?
  $node->revision = 0;    
  $node->body[$node->language][0]['value']   = $hn_file_content;
  $node->body[$node->language][0]['summary'] = text_summary($hn_file_content);
  $node->body[$node->language][0]['format']  = $input_format_machine_name; // not consistent across all installations. see note above.
  // $node->path['alias'] = $hn_url_alias;  DO NOT USE. Always adds a new record, never updates.
  $node->field_hypernode_namespace[$node->language][0]['value'] = rtrim($hn_url_prefix . $hn_namespace, "/");
  
  if ($hn_update_mode == "add"){
    $node = node_submit($node); // Prepare node for saving
  }  

  node_save($node);
  
  // add/update url alias.
  // Note: do not use "$node->path['alias'] = $hn_url_alias;" - it always adds a new record in url_alias.
  // Note: $node->pid no longer available in D7, therefore db_query is needed.
  // http://api.drupal.org/api/drupal/modules!path!path.test/function/PathTestCase%3A%3AgetPID/7
  $path = array(
    'source' => 'node/' . $node->nid,
    'alias' => $hn_url_alias,
    'pid' => db_query("SELECT pid FROM {url_alias} WHERE alias = :alias", array(':alias' => $hn_url_alias))->fetchField(),
    'language' => $node->language,
  );
  path_save($path);
   
  // drupal_set_message( "Node with nid " . $node->nid . " saved!\n");
  // drupal_set_message($hn_file_content);
  // drupal_set_message("alias:" . $hn_url_alias . " -namespace:" . $hn_namespace . " -title:" . $hn_node_title);
  return TRUE;
}