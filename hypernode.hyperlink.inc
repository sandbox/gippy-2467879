<?php
  $hn_mapping;
  $hn_ignore_nodes;
  $hn_delimiters_before;
  $hn_delimiters_after;
	$hn_url_replacements;

/**
 * Hyperlink nodes.
 */
function hypernode_hyperlink_run ($form, $form_state) {
  GLOBAL $hn_mapping;

  if(hypernode_hyperlink_set_vars($form, $form_state) == FALSE) {
    return FALSE;
  }

  // Parse the input mapping and place into $hn_map array
  // dpm($hn_mapping);
  // drupal_set_message(bin2hex($hn_mapping));
  $tok = strtok($hn_mapping, "\r\n");
  while ($tok !== false) {
    $tok2 = trim($tok);
    if ((strpos($tok2, "=>")) > 0){
      $hn_map[] = array(
        'content_dir' => trim(substr($tok2, 0, (strpos($tok2, "=>")))),
        'reference_dir' => trim(substr($tok2, (strpos($tok2, "=>")) +2)),
      );
    }
    $tok = strtok("\r\n");
  }
  // dpm($hn_map);

  // Process the mapping
	foreach($hn_map as $map) {
    hypernode_hyperlink_process_map($map['content_dir'], $map['reference_dir']);
	}

  return TRUE;
}


/**
 * Set variables
 */
function hypernode_hyperlink_set_vars($form, $form_state) {
  GLOBAL $hn_mapping;
  GLOBAL $hn_ignore_nodes;

  GLOBAL $hn_delimiters_before;
  GLOBAL $hn_delimiters_after;

	GLOBAL $hn_url_replacements;

  $hn_mapping = "";
  $hn_ignore_nodes = "";

  $hn_mapping = $form_state['values']['hypernode_hyperlink_mapping'];
  variable_set('hypernode_hyperlink_mapping', $hn_mapping);

  $hn_ignore_nodes = $form_state['values']['hypernode_hyperlink_ignore_nodes'];
  variable_set('hypernode_hyperlink_ignore_nodes', $hn_ignore_nodes);

  $delimiter_before = $form_state['values']['hypernode_hyperlink_delimiter_before'];
  variable_set('hypernode_hyperlink_delimiter_before', $delimiter_before);

  $delimiter_after = $form_state['values']['hypernode_hyperlink_delimiter_after'];
  variable_set('hypernode_hyperlink_delimiter_after', $delimiter_after);

  // escaped characters are passed in as a literal string. Need to change them to control characters.
  $delimiter_before = str_replace('\n', "\n", $delimiter_before);
  $delimiter_before = str_replace('\r', "\r", $delimiter_before);
  $hn_delimiters_before = explode('^^', trim($delimiter_before, "^^"));

  $delimiter_after = str_replace('\n', "\n", $delimiter_after);
  $delimiter_after = str_replace('\r', "\r", $delimiter_after);
	$hn_delimiters_after = explode('^^', trim($delimiter_after, "^^"));

  // dpm($hn_delimiters_before);
  // dpm($hn_delimiters_after);



// HACK. Build an array as if it were an input field on the Hypernode Config form
  $hn_url_replacements = "";
// HACK. HARDCODING.
  // $hn_url_replacements = $form_state['values']['hypernode_hyperlink_url_replacements'];
  // variable_set('hypernode_hyperlink_url_replacements', $hn_url_replacements);
	$hn_url_replacements[] = array(
    'url_text' => 'what-are-you',
    'node_text' => 'what are you?',
  );
  // drupal_set_message($hn_url_replacements[0]["url_text"] . '--' . $hn_url_replacements[0]["node_text"] . ' was set');


  return TRUE;
}

/**
 * Process content documents in a map for linking to reference documents
 */
function hypernode_hyperlink_process_map($content_dir, $reference_dir) {

  GLOBAL $hn_url_replacements;

  drupal_set_message($content_dir . " nodes successfully hyperlinked to " . $reference_dir . " nodes.");

  // the domain is the last element of the path, before the actual title
  if(strrpos($reference_dir, '/') === false){
    $ref_domain = $reference_dir;
  }
  else{
    $ref_domain = substr($reference_dir, strrpos($reference_dir, '/') +1);
  }

  // generate an array of node urls that may be linked to
  $sql = 'SELECT alias FROM {url_alias} a WHERE a.alias LIKE :alias';
  $dir = $reference_dir . '%';
  $result = db_query($sql, array(':alias' => $dir));
  foreach ($result as $record){
    // drupal_set_message($record->alias);
    $path = $record->alias;
    if(strrpos($path, '/') === false){
      $key = $path;
    }
    else{
      $key = substr($path, strrpos($path, '/') +1);
    }
    $ref_keys[] = array(
      'key' => $key,
      'path' => $path,
    );
    // drupal_set_message($key . '--' . $path);
	}

	// add to this array node urls from the replacement list
	foreach($ref_keys as $ref_key)
	{
	  $key = $ref_key['key'];
		$path = $ref_key['path'];

		// does this node url contain a special string from the Hypernode Config form?
    // if so, add another key using the special string in the node url
		foreach($hn_url_replacements as $hn_url_replacement)
	  {
      if (strpos($key, $hn_url_replacement["url_text"]) !== false){
        $key2 = str_replace($hn_url_replacement["url_text"], $hn_url_replacement["node_text"], $key);
        $ref_keys[] = array(
          'key' => $key2,
          'path' => $path,
        );
        // drupal_set_message($key . '--' . $path . ' replaced with: ' . $key2);
		  }
		}
	}

	// add to this array node urls for each page of a page range
	foreach($ref_keys as $ref_key)
	{
	  $key = $ref_key['key'];
		$path = $ref_key['path'];

    // does the key end with a pattern of -nnnn-nnnn? If so, this is a page range.
		// add: key = nn   path = path#nn for every one in the range
		$pattern = '/-[0123456789]+-[0123456789]+$/';
		if (preg_match($pattern, $key, $matches) > 0){
			$key2 = substr ( $key, 0, strlen($key) - strlen($matches[0]) );
      $temp_array = explode("-", $matches[0]);
		  $page_start = $temp_array[1];
		  $page_end = $temp_array[2];
      // drupal_set_message($key . ' -- ' . $key2 . ' -- ' . $matches[0] . ' -- ' . $page_start . ' -- ' . $page_end);
      for ($i = (int) $page_start; $i <= (int) $page_end; $i++) {
				$key2 = str_replace("-", " ", $key2);
				$ref_keys[] = array(
        'key' => $key2 . " " . $i,
        'path' => $path . "#" . $i,
         );
         // drupal_set_message('Added: ' . $key2 . " " . $i . ' === added: ' . $path . "#" . $i);
      }
		}
	}

	// add to this array node urls without hyphens or underscores.
  foreach($ref_keys as $ref_key)
	{
	  $key = $ref_key['key'];
		$path = $ref_key['path'];

    // does the key contain a hyphen or underscore? hyperlinked text will not contain them.
    // if so, add another key with them changed to spaces
    if (strpos($key, '_') !== false){
      $key2 = str_replace("_", " ", $key);
      $ref_keys[] = array(
        'key' => $key2,
        'path' => $path,
      );
      // drupal_set_message($key2 . "---" . $path . "was added to the reference keys");
    }
    if (strpos($key, '-') !== false){
      $key2 = str_replace("-", " ", $key);
      $ref_keys[] = array(
        'key' => $key2,
        'path' => $path,
      );
      // drupal_set_message($key2 . "---" . $path . "was added to the reference keys");
    }
  }

  // process every node in the content directory
  $sql = 'SELECT alias FROM {url_alias} a WHERE a.alias LIKE :alias';
  $dir = $content_dir . '%';
  $result = db_query($sql, array(':alias' => $dir));
  foreach ($result as $record){
    // drupal_set_message($record->alias);
    // $path = $record->alias;
    // drupal_set_message($path);
    hypernode_hyperlink_set_link($record->alias, $ref_domain, $ref_keys);

  }
  return TRUE;
}

/**
 * Process a single node
 */
function hypernode_hyperlink_set_link ($content_path, $ref_domain, $ref_keys)
{
  // drupal_set_message($content_path . '---' . $ref_domain);

  GLOBAL $hn_ignore_nodes;
  GLOBAL $hn_delimiters_before;
  GLOBAL $hn_delimiters_after;
  $node_update = FALSE;  // change to TRUE if a link is inserted

  // open the node
  $normal_path = drupal_get_normal_path($content_path);
  if ($normal_path == $content_path){
    drupal_set_message(t("Page not updated. Unable to get node ID for:" . $content_path), 'error');
    return FALSE;
  }
  else{
    $nid = substr($normal_path, strlen("node/"));  //example: node/2918
    $node = node_load($nid);
    if($node == FALSE){
      drupal_set_message(t("Page not updated. Unable to open node ID:" . $nid . " from URL alias:" . $content_path), 'error');
      return FALSE;
    }
  }

  // get the node content
  $node_body = $node->body[$node->language][0]['value'];

// $tag_value = $ref_key['key']
// $tag = $ref_key['key']
// $fileContent = $node_body

  // are any of the reference keys found in the body of the node?
	foreach($ref_keys as $ref_key)
	{
		// ignore reference keys starting with a "." Should not happen, but is possible the import picked up a hidden file
    if (substr($ref_key['key'], 0, 1) == "."){
      continue;
    }

   	// don't allow a node to link to itself
		if ($content_path == $ref_key['path']){
      // drupal_set_message($content_path . '---' . $ref_key['path'] . '--- would link to itself');
      continue;
    }

    // bypass keys that the user has said to ignore
    if (strpos($hn_ignore_nodes, $ref_key['path']) !== FALSE){
      // drupal_set_message($ref_key['path'] . '--- is ignored and is being bypassed');
      continue;
    }

		// Start search on 2nd line.  Do not make a link on the header line.
		$tagpos = stripos($node_body, "\n");
		$tagpos++;
		$tagpos = -1;		// try out allowing hyperlinking on 1st line

		// Do not make a search on the final line
		$lastlinepos = strrpos($node_body, "\n", -2);

if ($ref_key['key'] == 'how i used truth 26') {
  // drupal_set_message ('tried on ' . $content_path);
}
		while (1)
		{
			// is the reference key found anywhere in the node body? If not, go to the next reference key.
      $tagpos ++;
			$tagpos = stripos($node_body, $ref_key['key'], $tagpos);
			if ($tagpos === false){
        break;
      }
			if ($tagpos > $lastlinepos){	// bypass final line
				break;
			}

if ($ref_key['key'] == 'how i used truth 26') {
  // drupal_set_message ('success on ' . $content_path);
}

// HACK: check if there is an earlier <h4> that is not closed by </h4>.
// TODO: need an input field in config to select for this.


			$linkbeg = strripos($node_body, "<h4", $tagpos - strlen($node_body));
			$linkend = strripos($node_body, "</h4>", $tagpos - strlen($node_body));
			if (($linkbeg !== false) and ($linkbeg > $linkend)){
        continue;
      }
			$linkbeg = strripos($node_body, "<h1", $tagpos - strlen($node_body));
			$linkend = strripos($node_body, "</h1>", $tagpos - strlen($node_body));
			if (($linkbeg !== false) and ($linkbeg > $linkend)){
        continue;
      }

			$linkbeg = strripos($node_body, "<h2", $tagpos - strlen($node_body));
			$linkend = strripos($node_body, "</h2>", $tagpos - strlen($node_body));
			if (($linkbeg !== false) and ($linkbeg > $linkend)){
        continue;
      }
			$linkbeg = strripos($node_body, "<h3", $tagpos - strlen($node_body));
			$linkend = strripos($node_body, "</h3>", $tagpos - strlen($node_body));
			if (($linkbeg !== false) and ($linkbeg > $linkend)){
        continue;
      }

      // check if there is an earlier <no-hypernode> that is not closed by </no-hypernode>. If so, this text should not be hyperlinked.
      // note: do not place <no-hypernoode> on first line. $tagpos is set to start on 2nd line!
      $linkbeg = strripos($node_body, "<no-hypernode", $tagpos - strlen($node_body));
      $linkend = strripos($node_body, "</no-hypernode>", $tagpos - strlen($node_body));
      if (($linkbeg !== false) and ($linkbeg > $linkend)){
        continue;
      }

			// check if there is an earlier <a href= that is not closed by </a>. If so, this text is in a hyperlink.
			$linkbeg = strripos($node_body, "<a href=", $tagpos - strlen($node_body));
			$linkend = strripos($node_body, "</a>", $tagpos - strlen($node_body));
			if (($linkbeg !== false) and ($linkbeg > $linkend)){
        // drupal_set_message($ref_key['path'] . '-- is part of a hyperlink url and is being bypassed');
        continue;
      }

			// check if there is a later <a href= that is not closed by </a>. If so, this is in a hyperlink.
      // this should never happen because the above routine should have rooted out this possibility.
			// TODO. removed because the <a NAME=72>Page 72</a> tag in books that have multiple pages triggered this
			// and caused the book to have no hyperlinking.
/*
			$linkbeg = stripos($node_body, "<a href=", $tagpos);
			$linkend = stripos($node_body, "</a>", $tagpos);
			if (($linkend !== false) and ($linkbeg > $linkend)){
        // drupal_set_message($ref_key['path'] . '-- is part of a hyperlink url and is being bypassed');
        continue;
      }
*/
      // make sure the term is not part of some larger text that should not be linked
      // do this my making sure that the characters preceding and following the term are in the list of delimiters
      if ( (!in_array( substr($node_body, $tagpos - 1, 1), $hn_delimiters_before )) or
           (!in_array( substr($node_body, $tagpos + strlen($ref_key['key']), 1), $hn_delimiters_after )) ){
        // drupal_set_message("bypassing: " . substr($node_body, $tagpos - 1, strlen($ref_key['key']) + 2) . " in: " . $content_path . " because of delimiters");
        continue;
      }

      // insert the html to change this text into a link
      $link_open = "<a href=\"/" . $ref_key['path'] . "\">";
      $link_close = "</a>";
      $node_body = substr_replace($node_body, $link_open, $tagpos, 0);
      $tagpos = $tagpos + strlen($link_open) + strlen($ref_key['key']);
      $node_body = substr_replace($node_body, $link_close, $tagpos, 0);
      $node_update = TRUE;
    }
  }
  if ($node_update){
    $node->body[$node->language][0]['value'] = $node_body;
    $node->body[$node->language][0]['summary'] = text_summary($node_body);
    node_save($node);
  }

}
