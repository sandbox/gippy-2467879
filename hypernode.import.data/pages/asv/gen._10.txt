===== Genesis 10 (asv) =====

<sup>10:1</sup>Now these are the generations of the sons of Noah, //namely//, of Shem, Ham, and Japheth: and unto them were sons born after the flood.

<sup>10:2</sup>The sons of Japheth: Gomer, and Magog, and Madai, and Javan, and Tubal, and Meshech, and Tiras. <sup>10:3</sup>And the sons of Gomer: Ashkenaz, and Riphath, and Togarmah. <sup>10:4</sup>And the sons of Javan: Elishah, and Tarshish, Kittim, and Dodanim. <sup>10:5</sup>Of these were the isles of the nations divided in their lands, every one after his tongue, after their families, in their nations.

<sup>10:6</sup>And the sons of Ham: Cush, and Mizraim, and Put, and Canaan. <sup>10:7</sup>And the sons of Cush: Seba, and Havilah, and Sabtah, and Raamah, and Sabteca; and the sons of Raamah: Sheba, and Dedan. <sup>10:8</sup>And Cush begat Nimrod: he began to be a mighty one in the earth. <sup>10:9</sup>He was a mighty hunter before Jehovah: wherefore it is said, Like Nimrod a mighty hunter before Jehovah. <sup>10:10</sup>And the beginning of his kingdom was Babel, and Erech, and Accad, and Calneh, in the land of Shinar. <sup>10:11</sup>Out of that land he went forth into Assyria, and builded Nineveh, and Rehoboth-ir, and Calah, <sup>10:12</sup>and Resen between Nineveh and Calah (the same is the great city). <sup>10:13</sup>And Mizraim begat Ludim, and Anamim, and Lehabim, and Naphtuhim, <sup>10:14</sup>and Pathrusim, and Casluhim (whence went forth the Philistines), and Caphtorim.

<sup>10:15</sup>And Canaan begat Sidon his first-born, and Heth, <sup>10:16</sup>and the Jebusite, and the Amorite, and the Girgashite, <sup>10:17</sup>and the Hivite, and the Arkite, and the Sinite, <sup>10:18</sup>and the Arvadite, and the Zemarite, and the Hamathite: and afterward were the families of the Canaanite spread abroad. <sup>10:19</sup>And the border of the Canaanite was from Sidon, as thou goest toward Gerar, unto Gaza; as thou goest toward Sodom and Gomorrah and Admah and Zeboiim, unto Lasha. <sup>10:20</sup>These are the sons of Ham, after their families, after their tongues, in their lands, in their nations.

<sup>10:21</sup>And unto Shem, the father of all the children of Eber, the elder brother of Japheth, to him also were children born. <sup>10:22</sup>The sons of Shem: Elam, and Asshur, and Arpachshad, and Lud, and Aram. <sup>10:23</sup>And the sons of Aram: Uz, and Hul, and Gether, and Mash. <sup>10:24</sup>And Arpachshad begat Shelah; and Shelah begat Eber. <sup>10:25</sup>And unto Eber were born two sons: The name of the one was Peleg. For in his days was the earth divided. And his brother's name was Joktan. <sup>10:26</sup>And Joktan begat Almodad, and Sheleph, and Hazarmaveth, and Jerah, <sup>10:27</sup>and Hadoram, and Uzal, and Diklah, <sup>10:28</sup>and Obal, and Abimael, and Sheba, <sup>10:29</sup>and Ophir, and Havilah, and Jobab: all these were the sons of Joktan. <sup>10:30</sup>And their dwelling was from Mesha, as thou goest toward Sephar, the mountain of the east. <sup>10:31</sup>These are the sons of Shem, after their families, after their tongues, in their lands, after their nations.

<sup>10:32</sup>These are the families of the sons of Noah, after their generations, in their nations: and of these were the nations divided in the earth after the flood.

----

[[gen._9|Genesis 9]]

